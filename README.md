# matthewgall/tools

matthewgall/tools - A set of simple utilities that simplifies the setup, maintainence and optimization of many of your favourite applications

## Licence
All the scripts included in this repository are licenced under the GNU General Public Licence Version 2.

    Copyright (C) 2013 Matthew Gall

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## Pre-requisites
This set of scripts requires the following prerequisites:

    mongoctl - sudo pip install mongoctl
    tarsnap - 

## Support
Need help? I'd be more than happy to! You can use any of the following methods:

* Create an issue over on the [issue tracker](https://bitbucket.org/matthewgall/tools/issues) (no registration required) with all the necessary steps to replicate the issue, and the environment you are using;