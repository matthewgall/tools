#!/usr/bin/python

##
# Copyright (C) 2013 Matthew Gall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##

# Deal with imports
import os
import argparse
from subprocess import call

def processBackup( hostname, port, username, password, database, directory ):
	
	# Have they specified a username, but not a password?
	if (username != "" and password == ""):
		print "You must specify a password to continue"
		exit

	# Have they specified a directory that doesn't exist?
	if ( os.path.isdir( str( directory[0] ) ) == False ):
		print 'ERROR: The directory ' + directory[0] + ' does not exist'
		exit

def main():
	parser = argparse.ArgumentParser(
		description='A utility that simplifies the backup of MongoDB databases using mongoctl'
	)

	# Server Information
	parser.add_argument(
		'server',
		metavar='hostname',
		nargs=1,
		help='the hostname of the MongoDB server to process'
	)
	parser.add_argument(
		'-n', '--port',
		metavar='port',
		nargs=1,
		default=27017,
		help='the port of the MongoDB server to connect to. default: 27017'
	)

	# Authentication Information
	parser.add_argument(
		'-u', '--username',
		metavar='username',
		nargs=1,
		required=False,
		help='the username of a user able to connect to this server'
	)
	parser.add_argument(
		'-p', '--password',
		metavar='password',
		nargs=1,
		required=False,
		help='the password of the user able to connect to this server'
	)

	# Database Information
	parser.add_argument(
		'database',
		metavar='database',
		nargs=1,
		help='the name of the collection to backup. If unspecified, all collections are backed up'
	)

	# Output Information
	parser.add_argument(
		'directory',
		metavar='directory',
		nargs=1,
		help='the path to the backup directory.'
	)
	args = parser.parse_args()
	processBackup(args.port, args.server, args.username, args.password, args.database, args.directory)

if __name__=="__main__":
	main()